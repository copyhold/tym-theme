<?php
use Roots\Sage\Assets;
?>
<?php the_post(); ?>

<map name="atarim">
<area shape="poly" coords="363,300,319,446,566,491,735,268,516,186,498,198,491,210,489,210" href="/matuka" />
<area shape="poly" coords="905,66,749,262,1156,341,1169,85,1168,85,1165,72" href="/yetzira" />
<area shape="poly" coords="259,59,905,10,734,248" href="/moreshet" />
<area shape="poly" coords="223,71,494,180,305,318,261,439,5,340,5,119,6,118,20,105,21,105,32,101,33,101,46,100" href="/iruim" />
</map>
<img id="mapatarim" src="<?php echo Assets\asset_path('images/bigmap.png');?>" alt="" usemap="atarim" />
<article class=text>
  <h1><?php the_title() ?></h1>
  <?php get_template_part('templates/content', 'page'); ?>
</article>
<section class="video-events">
  <iframe src="https://www.youtube.com/embed/fLrpeMQinpE" frameborder="0" allowfullscreen></iframe>
  <h2>אירועים וחדשות</h2>
<section class="events-news">
<?php
$today = strtotime('today'); 

$vsel_meta_query = array( 
  'relation' => 'AND',
  array( 
    'key' => 'event-date', 
    'value' => $today, 
    'compare' => '>=', 
    'type' => 'NUMERIC'
  ) 
); 

$vsel_query_args = array( 
  'post_type' => 'event', 
  'post_status' => 'publish', 
  'ignore_sticky_posts' => true, 
  'meta_key' => 'event-date', 
  'orderby' => 'meta_value_num', 
  'order' => 'asc',
  'posts_per_page' => 2,
  'paged' => 0, 
//  'meta_query' => $vsel_meta_query
); 

$vsel_events = new WP_Query( $vsel_query_args );

if ( $vsel_events->have_posts() ) : 
  while( $vsel_events->have_posts() ): $vsel_events->the_post(); 
    $event_start_date = date('d/m', get_post_meta( get_the_ID(), 'event-start-date', true ));
    $event_date       = date('d/m', get_post_meta( get_the_ID(), 'event-date', true ));
    $event_time       = get_post_meta( get_the_ID(), 'event-time', true );
    $widget_info      = esc_attr(get_option('vsel-setting-7'));
    $title            = get_the_title();
    $content          = apply_filters( 'the_content', get_the_content() );
    $phones           = do_shortcode('[phones]');
    if ( has_post_thumbnail() ) { 
      $image = get_the_post_thumbnail( null, 'post-thumbnail', array('class' => 'vsel-image') ); 
    }
    echo <<<EOB
<article class="event">
  $image
  <h5>$title</h5>
  <div>$content</div>
  <div>
    <strong>פרטים נוספים</strong>
    $phones
  </div>
</article>
EOB;
  endwhile;
endif;
?>
<article class="news">
<strong>מה חדש</strong>
<?php
$args = array( 'posts_per_page' => 5, 'category_name' => 'news' );
$news = get_posts( $args );
foreach ( $news as $post ) : setup_postdata( $post ); ?>
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
<?php 
endforeach; 
wp_reset_postdata();
?>
</article>
</section>
</section><!-- end video and news -->
<section class="gallery">
<?php
$gallery_post = get_page_by_title( 'home page gallery', OBJECT, 'post' );
$gallery = do_shortcode($gallery_post->post_content);
echo $gallery;
?>
</section>
<div id="viewbigimage"></div>

