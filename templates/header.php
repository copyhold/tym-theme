<header class="top">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
    <nav class="social">
      <fieldset class="change-font">
        <button>א</button>
        <button>א</button>
      </fieldset>
      <?php echo do_shortcode('[phones]'); ?>
      <a class="social wa" href="#"></a>
      <a class="social yo" href="#"></a>
      <a class="social in" href="#"></a>
      <a class="social fb" href="#"></a>
      <a class="lang" href="/en">en</a>
    </nav>
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
  </div>
</header>
