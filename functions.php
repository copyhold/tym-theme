<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function phones_shortcode() {
  return <<<EOB
<span class="phones"><a href="tel:08-6720559">08-6720559</a><a href="tel:052-3923104">052-3923104</a></span>
EOB;
}
add_shortcode('phones', 'phones_shortcode');


// EDITOR
function onhold_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'mce_buttons_2', 'onhold_mce_buttons_2' );
function tym_tiny_mce_before_init( $settings ) {
	$style_formats = array(
		array( 'title' => 'Two white blocks',  'wrapper'=>true, 'block' => 'div', 'classes' => 'two-white-blocks' ),
		array( 'title' => 'White BG block',  'wrapper'=>true, 'block' => 'div', 'classes' => 'white-bg-block' ),
		array( 'title' => 'Block with image','wrapper'=>true, 'block' => 'div', 'classes' => 'block-with-image' ),
		array( 'title' => 'Full weight colored bar', 'block' => 'h4', 'classes' => 'full-weight-header' ),
		array( 'title' => 'Orange header', 'block' => 'h4', 'classes' => 'orange-header' ),
		array( 'title' => 'Image separator', 'block' => 'hr', 'classes' => 'image-separator' ),
	);
	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
}
add_filter( 'tiny_mce_before_init', 'tym_tiny_mce_before_init' );

function wpdocs_theme_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
// EDITOR

function tym_prebuilt_layouts($layouts){
  global $about_layout;
  $layouts['home-page'] = array(
    // We'll add a title field
    'name' => __('about page', 'vantage'),    // Required
    'description' => __('About and others page with 3 sections', 'vantage'),    // Optional
    'widgets' => $about_layout['widgets'],
    'grids' => $about_layout['grids'],
    'grid_cells' => $about_layout['grid_cells']
  );
  $layouts['matok-page'] = array(
    'name' => __('havaya metuka', 'vantage'),    // Required
    'description' => __('About and others page with 3 sections', 'vantage'),    // Optional
    'widgets'    => $matokpage['widgets'],
    'grids'      => $matokpage['grids'],
    'grid_cells' => $matokpage['grid_cells']
  );
  return $layouts;

}
add_filter('siteorigin_panels_prebuilt_layouts','tym_prebuilt_layouts');
$about_layout = array(
  'widgets' => 
  array (
    0 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca49dbb3f',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 0,
        'cell' => 0,
        'id' => 0,
        'widget_id' => '1ae3b6e5-8195-4252-9d48-1a691cfdddcc',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    1 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca436e8fc',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 0,
        'cell' => 0,
        'id' => 1,
        'widget_id' => 'd2c41f8d-f626-44b7-9ea2-8364e17b6af9',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    2 => 
    array (
      'title' => 'none',
      'text' => 'text for example',
      'filter' => true,
      'panels_info' => 
      array (
        'class' => 'WP_Widget_Text',
        'raw' => false,
        'grid' => 0,
        'cell' => 1,
        'id' => 2,
        'widget_id' => 'a14568af-9b8d-4c2b-a11e-44dac031cf48',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    3 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746caa81e472',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 0,
        'id' => 3,
        'widget_id' => 'a0305d76-74df-4e25-96a4-dbf0c75817de',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    4 => 
    array (
      'title' => '',
      'text' => '',
      'text_selected_editor' => 'tinymce',
      'autop' => true,
      '_sow_form_id' => '5746caabba694',
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Editor_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 0,
        'id' => 4,
        'widget_id' => 'dca35a7e-06a8-4a3b-b685-a239a2e6eeef',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    5 => 
    array (
      'title' => '',
      'text' => '',
      'filter' => false,
      'panels_info' => 
      array (
        'class' => 'WP_Widget_Text',
        'raw' => false,
        'grid' => 1,
        'cell' => 1,
        'id' => 5,
        'widget_id' => '5e83f715-c182-402a-8af6-644531b8c715',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    6 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca9d83e2a',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 1,
        'id' => 6,
        'widget_id' => 'a50ff3e4-2a71-4a53-b4b4-7f3327fe6183',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    7 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 2,
        'cell' => 0,
        'id' => 7,
        'widget_id' => '1ae3b6e5-8195-4252-9d48-1a691cfdddcc',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    8 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 2,
        'cell' => 0,
        'id' => 8,
        'widget_id' => 'd2c41f8d-f626-44b7-9ea2-8364e17b6af9',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    9 => 
    array (
      'title' => '',
      'text' => '',
      'text_selected_editor' => 'tinymce',
      'autop' => true,
      '_sow_form_id' => '5746cbb345803',
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Editor_Widget',
        'grid' => 2,
        'cell' => 1,
        'id' => 9,
        'widget_id' => '011898ed-41bd-4eff-8fc9-c18c02b7388e',
        'style' => 
        array (
          'background_image_attachment' => false,
          'background_display' => 'tile',
        ),
      ),
    ),
  ),
  'grids' => 
  array (
    0 => 
    array (
      'cells' => 2,
      'style' => 
      array (
      ),
    ),
    1 => 
    array (
      'cells' => 2,
      'style' => 
      array (
        'class' => 'orange title and white bg',
        'row_stretch' => 'full',
        'background_display' => 'tile',
      ),
    ),
    2 => 
    array (
      'cells' => 2,
      'style' => 
      array (
      ),
    ),
  ),
  'grid_cells' => 
  array (
    0 => 
    array (
      'grid' => 0,
      'weight' => 0.5,
    ),
    1 => 
    array (
      'grid' => 0,
      'weight' => 0.5,
    ),
    2 => 
    array (
      'grid' => 1,
      'weight' => 0.5,
    ),
    3 => 
    array (
      'grid' => 1,
      'weight' => 0.5,
    ),
    4 => 
    array (
      'grid' => 2,
      'weight' => 0.5,
    ),
    5 => 
    array (
      'grid' => 2,
      'weight' => 0.5,
    ),
  ),
);
$matokpage = array (
  'widgets' => 
  array (
    0 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca49dbb3f',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 0,
        'cell' => 0,
        'id' => 0,
        'widget_id' => '1ae3b6e5-8195-4252-9d48-1a691cfdddcc',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    1 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca436e8fc',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 0,
        'cell' => 0,
        'id' => 1,
        'widget_id' => 'd2c41f8d-f626-44b7-9ea2-8364e17b6af9',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    2 => 
    array (
      'title' => 'ההךללח',
      'text' => 'ךל לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות לחלק אוטומטית לפסקאות',
      'filter' => true,
      'panels_info' => 
      array (
        'class' => 'WP_Widget_Text',
        'raw' => false,
        'grid' => 0,
        'cell' => 1,
        'id' => 2,
        'widget_id' => 'a14568af-9b8d-4c2b-a11e-44dac031cf48',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    3 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746caa81e472',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 0,
        'id' => 3,
        'widget_id' => 'a0305d76-74df-4e25-96a4-dbf0c75817de',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    4 => 
    array (
      'title' => '',
      'text' => '',
      'text_selected_editor' => 'tinymce',
      'autop' => true,
      '_sow_form_id' => '5746caabba694',
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Editor_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 0,
        'id' => 4,
        'widget_id' => 'dca35a7e-06a8-4a3b-b685-a239a2e6eeef',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    5 => 
    array (
      'title' => '',
      'text' => '',
      'filter' => false,
      'panels_info' => 
      array (
        'class' => 'WP_Widget_Text',
        'raw' => false,
        'grid' => 1,
        'cell' => 1,
        'id' => 5,
        'widget_id' => '5e83f715-c182-402a-8af6-644531b8c715',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    6 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      '_sow_form_id' => '5746ca9d83e2a',
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 1,
        'cell' => 1,
        'id' => 6,
        'widget_id' => 'a50ff3e4-2a71-4a53-b4b4-7f3327fe6183',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    7 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 2,
        'cell' => 0,
        'id' => 7,
        'widget_id' => '1ae3b6e5-8195-4252-9d48-1a691cfdddcc',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    8 => 
    array (
      'image' => 0,
      'image_fallback' => '',
      'size' => 'full',
      'align' => 'default',
      'title' => '',
      'title_position' => 'hidden',
      'alt' => '',
      'url' => '',
      'bound' => true,
      'new_window' => false,
      'full_width' => false,
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Image_Widget',
        'raw' => false,
        'grid' => 2,
        'cell' => 0,
        'id' => 8,
        'widget_id' => 'd2c41f8d-f626-44b7-9ea2-8364e17b6af9',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
    9 => 
    array (
      'title' => '',
      'text' => '',
      'text_selected_editor' => 'tinymce',
      'autop' => true,
      '_sow_form_id' => '5746cbb345803',
      'panels_info' => 
      array (
        'class' => 'SiteOrigin_Widget_Editor_Widget',
        'raw' => false,
        'grid' => 2,
        'cell' => 1,
        'id' => 9,
        'widget_id' => '011898ed-41bd-4eff-8fc9-c18c02b7388e',
        'style' => 
        array (
          'background_display' => 'tile',
        ),
      ),
    ),
  ),
  'grids' => 
  array (
    0 => 
    array (
      'cells' => 2,
      'style' => 
      array (
      ),
    ),
    1 => 
    array (
      'cells' => 2,
      'style' => 
      array (
        'class' => 'orange title and white bg',
        'row_stretch' => 'full',
        'background_display' => 'tile',
      ),
    ),
    2 => 
    array (
      'cells' => 2,
      'style' => 
      array (
      ),
    ),
  ),
  'grid_cells' => 
  array (
    0 => 
    array (
      'grid' => 0,
      'weight' => 0.5,
    ),
    1 => 
    array (
      'grid' => 0,
      'weight' => 0.5,
    ),
    2 => 
    array (
      'grid' => 1,
      'weight' => 0.5,
    ),
    3 => 
    array (
      'grid' => 1,
      'weight' => 0.5,
    ),
    4 => 
    array (
      'grid' => 2,
      'weight' => 0.5,
    ),
    5 => 
    array (
      'grid' => 2,
      'weight' => 0.5,
    ),
  ),
);
